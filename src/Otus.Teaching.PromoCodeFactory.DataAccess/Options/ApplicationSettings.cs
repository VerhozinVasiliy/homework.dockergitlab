﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Options
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; }
    }
}
